<?php

namespace app\controllers;

use yii\rest\ActiveController;

class DepartController extends ActiveController {

    public $modelClass = 'app\models\Departamento'; // colocamos el modelo de la tabla que vamos a utilizar

}
